//-----------------------------------------------------------------------------
//Start of file 'build.sbt'
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
lazy val authorList =         SettingKey[String]("authorList")
lazy val license =            SettingKey[String]("license")
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name := "remoteUpdater"
  , description :=        "Remote updater"
  , organization :=       "IAA-CSIC"
  , authorList  :=        "Rafael Morales (rmorales@iaa.es)"
  , homepage :=           Some(url("http://www.iaa.csic.es"))
  , license :=            "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.remoteUpdater.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
)
//=============================================================================
//versions
scalaVersion := "2.12.17"

val apacheLoggingVersion     = "2.16.0"
val apacheCommonFileUtils    = "2.6"
val commandLineParserVersion = "3.1.5"
val qosLogbackVersion        = "1.3.1"
val typeSafeVersion          = "1.3.3"
//=============================================================================
//merge strategy
//(remember ti update properly the file: project/plugin.sbt
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.last
}
//=============================================================================
//dependence list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://github.com/apache/logging-log4j-scala
  , "org.apache.logging.log4j" % "log4j-api" % apacheLoggingVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLoggingVersion
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion

  //command line argument parser: https://github.com/scallop/scallop
  ,  "org.rogach" %% "scallop" % commandLineParserVersion

  //apache commons file utils
  , "commons-io" % "commons-io" % apacheCommonFileUtils

  //git repo manager: https://www.eclipse.org/jgit/download/
  , "org.eclipse.jgit" % "org.eclipse.jgit" % "5.5.1.201910021850-r"

  //data transferring: http://www.jcraft.com/jsch/
  , "com.jcraft" % "jsch" % "0.1.55"
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= dependenceList)
  .settings(mainClass in (assembly) := Some(mainClassName))
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------