/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/nov/2018
 * Time:  02h:57m
 * Description: None
 */
//=============================================================================
//=============================================================================
//File: myFile.scala
//=============================================================================
/** It implements basic operation on a file
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.util.file
//=============================================================================
// System import sectionName
//=============================================================================
import com.common.logger.VisualLazyLogger

import java.io._
import java.text.SimpleDateFormat
import java.util.concurrent.atomic.AtomicBoolean
import java.util.Date
import java.io.{BufferedWriter, FileWriter}
import java.nio.file.Path
//=============================================================================
import com.common.util.path.Path._
import com.common.util.time.Time._
//=============================================================================
//=============================================================================
object MyFile {
  //---------------------------------------------------------------------------
  val PathYear=new SimpleDateFormat("yyyy")
  val PathYearMonth=new SimpleDateFormat("yyyy'_'MM")
  val PathYearMonthDay=new SimpleDateFormat("yyyy'_'MM'_'dd")
  //---------------------------------------------------------------------------
  private val ExtensionPattern = """\.[A-Za-z0-9]+$""".r
  //---------------------------------------------------------------------------
  def copyFile(src: String, dest: String): Path = {
    import java.nio.file.StandardCopyOption.REPLACE_EXISTING
    import java.nio.file.Files.copy
    import java.nio.file.Paths.get
    import scala.language.implicitConversions
    implicit def toPath (filename: String): Path = get(filename)
    copy (src, dest, REPLACE_EXISTING)
  }
  //-------------------------------------------------------------------------
  def getFileListAndSize(dir: String, suffix: String=""): StringBuilder = {

    val s = new StringBuilder
    val d = new File(dir)

    if (!d.exists){
      println (s"Error, directory '$dir' does not exist")
      return s
    }
    if (!d.isDirectory){
      println (s"Error, directory '$dir' is not a directory")
      return s
    }
    d.listFiles.filter(_.isFile).foreach { f => s.append(f.getName+"\t"+f.length+suffix) }
    s
  }
  //-------------------------------------------------------------------------
  /** Returns if the input path is a file
   * @param s Path to check
   * @return True if input is a file, false in other case
   */
  def fileExist(s:String) : Boolean =
  {
    val f = new File(s)
    f.exists && !f.isDirectory
  }
  //-------------------------------------------------------------------------
  def deleteFile(path: String): Boolean =  new File(path).delete()
  //-------------------------------------------------------------------------
  def deleteFileIfExist(path: String): Boolean =
    if (fileExist(path)) new File(path).delete()
    else true
  //-------------------------------------------------------------------------
  def getFileNameNoPathNoExtension (s: String) : String = removeFileExtension(getOnlyFilename(s))
  //-------------------------------------------------------------------------
  def fileByteSize(s:String): Long = new File(s).length()
  //-------------------------------------------------------------------------
  /** Returns a new string that ends with the provided suffix
   * @param s MyString to process
   * @param suffix to append if it necessary
   * @return Returns a new string that ends with the provided suffix
   */
  def ensureFileExtension(s:String,suffix: String) : String = if (s.endsWith(suffix)) s else s+suffix
  //-------------------------------------------------------------------------
  def hasFileExtension(fileName: String, extensions: List[String]): Boolean =
    extensions.map { _.toLowerCase }.exists { fileName.toLowerCase endsWith "." + _ }
  //-------------------------------------------------------------------------
  def getFileExtension(s: String): String = ExtensionPattern findFirstIn s getOrElse ""
  //-------------------------------------------------------------------------
  def removeFileExtension(s: String): String = {
    if (s.indexOf(".") == -1)  s
    else s.slice(s.lastIndexOf("/")+1,s.lastIndexOf("."))
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
case class MyFile(rootPath: String
                  , innmediateWrite : Boolean = false
                  , extension : String = ".txt"
                  , namePrefix : String = ""
                  , nameSuffix : String = "") extends VisualLazyLogger {  //true byteSeq is buffered before effective write

  //-------------------------------------------------------------------------
  // Variable declaration
  //-------------------------------------------------------------------------
  private var name : String ="Not set"
  private var writer : BufferedWriter=_
  private val closed = new AtomicBoolean(false)

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  init(rootPath)

  //-------------------------------------------------------------------------
  private def createFileAndDirectoryByDate(path: String
                                           ,  extension: String
                                           ,  namePrefix: String
                                           ,  nameSuffix: String
                                           ,  createWriter: Boolean = true) : (String, BufferedWriter)={
    //get time stamp
    val now = new Date()

    //create path of directories  based on date
    ensureDirectoryExist(path)

    //directory tree
    val finalDirectory=ensureEndWithFileSeparator(path)+
      PathYear.format(now) + FileSeparator +
      PathYearMonth.format(now) + FileSeparator +
      PathYearMonthDay.format(now)+ FileSeparator

    //build directory list
    ensureDirectoryExist(finalDirectory)

    //build file name
    val fileName=finalDirectory+
      namePrefix+
      getISO_DateLocalTimeStampWindowsCompatible()+
      nameSuffix+
      extension

    info( s"Using for logging: '$fileName'" )
    closed.set(false)

    if (createWriter)
      ( fileName, new BufferedWriter( new FileWriter( fileName )))
    else
      ( fileName, null)
  }

  //-------------------------------------------------------------------------
  private def init(s:String) : Unit = {

    synchronized{
      val result = createFileAndDirectoryByDate( rootPath, extension, namePrefix, nameSuffix )
      name = result._1
      writer = result._2
    }
  }

  //-------------------------------------------------------------------------
  def getName: String = name
  //-------------------------------------------------------------------------
  def getAbsolutePath: String = new File(name).getAbsolutePath
  //-------------------------------------------------------------------------
  def write(s:String) : Boolean = {
    try {
      synchronized{
        writer.write(s)
        if (innmediateWrite) flush()
      }
      true
    }
    catch {
      case e: Exception => error(s"Error writing string '$s' in file '$name'")
        false
    }
  }
  //-------------------------------------------------------------------------
  def writeLine(s:String): Boolean = write(s + LineSeparator)
  //-------------------------------------------------------------------------
  def close() : Boolean = {

    if(closed.get){
      printf(s"File '$name' is already closed")
      return false
    }

    try {
      synchronized{
        writer.flush()
        writer.close()
      }
      closed.set(true)
      true
    }
    catch {
      case e: Exception => error(s"Error closing file '$name'")
        closed.set(true)
        false
    }
  }
  //-------------------------------------------------------------------------
  def reStart(initialMessage: String = "") : Unit = {

    synchronized{
      close()
      init(rootPath)
      if(!initialMessage.isEmpty) write(initialMessage)
    }
  }
  //-------------------------------------------------------------------------
  def flush(): Unit = synchronized{ writer.flush() }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'MyFile'

//=============================================================================
// End of 'File.scala' file
//=============================================================================
