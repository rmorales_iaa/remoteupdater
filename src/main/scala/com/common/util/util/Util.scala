//=============================================================================
package com.common.util.util
//=============================================================================
import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import sys.process._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}
import java.math.BigInteger
//=============================================================================
//=============================================================================
object Util {
  //---------------------------------------------------------------------------
  final val SO_FILE_SEPARATOR = java.io.File.separator
  final val OsName=System.getProperty("os.name").toLowerCase
  //---------------------------------------------------------------------------
  def isOsWindows = OsName.contains("win")
  def isOsMac = OsName.contains("mac")
  def isUnix = OsName.contains("nix") || OsName.contains("nux") || OsName.contains("aix")
  def isOsSolaris = OsName.contains("sunos")
  //---------------------------------------------------------------------------
  //(physical core count, thread core count)
  def getCoreCount(): (Int, Int) = {
    if (!isUnix) {
      println("Error, this operating system is not Unix/linux. Can not get the core count")
      return (0, 0)
    }
    var physicalCoreCount = 0
    var totalCoreCount = 0
    runShellCommandAndGetStringOutput("lscpu").split("\n").foreach { line =>
      if (line.startsWith("CPU(s):"))
        totalCoreCount = line.replace("CPU(s):", "").trim.toInt
      if (line.startsWith("Core(s) per socket:"))
        physicalCoreCount = line.replace("Core(s) per socket:", "").trim.toInt
    }
    (physicalCoreCount, totalCoreCount)
  }
  //---------------------------------------------------------------------------
  // see https://stackoverflow.com/questions/40338975/scala-flatten-list-of-string-and-liststring
  def flattenList(xs: List[Any]): List[Any] =
    xs match {
      case Nil => Nil
      case (ys: List[_]) :: t => flattenList(ys) ::: flattenList(t)
      case h :: t => h :: flattenList(t)
    }

  def flattenSeq(xs: Seq[Any]): Seq[Any] =
    xs match {
      case Nil => Nil
      case (ys: Seq[_]) :: t => flattenSeq(ys) ++ flattenSeq(t)
      case h :: t => h +: flattenSeq(t)
    }
  //---------------------------------------------------------------------------
  def getEvenFromSeq[T](a: Seq[T]): List[T] = getOddFromSeq(a.drop(1))
  //---------------------------------------------------------------------------
  def getOddFromSeq[T](a: Seq[T]): List[T] = a.sliding(1, 2).toList.flatten
  //---------------------------------------------------------------------------
  def getN_ItemList[T](a: Seq[T], n: Int): List[T] = a.sliding(1, n).toList.flatten
  //---------------------------------------------------------------------------
  def toHexString(l: Seq[Byte]
                  , startingDivider:String = ""
                  , endDivider:String = ""): String =
    l.map{ b => String.format("%02X", new java.lang.Integer(b & 0xff)) }
      .mkString(startingDivider, sep =" ", endDivider)
  //---------------------------------------------------------------------------
  def garbageCollectorRequest() = sys.runtime.gc()
  //---------------------------------------------------------------------------
  def getComputerName: String = {
    import java.net.InetAddress
    var localHost : InetAddress = null
    Try {
      localHost = InetAddress.getLocalHost
    }
    match {
      case Success(_) => localHost.getHostName
      case Failure(_) => "No Name"
    }
  }
  //---------------------------------------------------------------------------
  def unsignedByteToInt(b: Byte): Int = b.toInt & 0xFF
  //---------------------------------------------------------------------------
  def runShellSimple(c: String) = {
    val command = Array("/bin/bash", "-c", c)
    val process = Runtime.getRuntime.exec(command,null,null)
    process.waitFor()
  }
  //---------------------------------------------------------------------------
  def runShellCommandAndGetStringOutput(command: String
                                        , parameterSeq: Seq[String] = Seq()
                                        , outputBufferSize: Int = 8096
                                        , waitForFinishCommand: Boolean = true
                                       ) = {
    val finalCommand = command + " " + parameterSeq.mkString(" ")
    val streamOut = new Array[Byte](outputBufferSize)
    val size = Util.runShellCommandAndGetBinaryOutput(finalCommand, streamOut, waitForFinishCommand)
    new String(streamOut.take(size))
  }
  //---------------------------------------------------------------------------
  def runShellWaitUntilTerminate(command: String, parameterSeq: Seq[String] = Seq()) = {
    val finalCommand = command + " " + parameterSeq.mkString(" ")
    val process = Runtime.getRuntime.exec(finalCommand)
    process.waitFor()
  }
  //---------------------------------------------------------------------------
  def runShellCommandAndGetBinaryOutput(command: String, stream: Array[Byte], waitForFinishCommand: Boolean = true) = {
    val process = Runtime.getRuntime.exec(command)
    if (waitForFinishCommand) process.waitFor()
    val stdout = process.getInputStream
    stdout.read(stream)
  }
  //---------------------------------------------------------------------------
  def runShellCommandAndGetBinaryOutput(command: String
                                        , streamIn: Array[Byte]
                                        , streamOut: Array[Byte]
                                        , streamErr: Array[Byte]) = {
    val process = Runtime.getRuntime.exec(command)
    val stdin = process.getOutputStream
    val stdout = process.getInputStream
    val stderr = process.getErrorStream

    stdin.flush
    stdin.write(streamIn)
    stdin.close

    stdout.read(streamOut)
    stderr.read(streamErr)
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/15975130/how-to-use-processbuilder-to-return-binary-data
  def runShellCommandAndGetBinaryOutput_2(command: String, streamIn: Array[Byte] = Array()) = {

    val stdout = new ByteArrayOutputStream
    val stdIn = new ByteArrayInputStream(streamIn)

    (command #< stdIn) #> stdout!! //!! means that it will wait until completion

    stdout.toByteArray
  }
  //---------------------------------------------------------------------------
  //https://www.scala-lang.org/old/node/7542
  def addDirToJavaLibraryPath(dir: String) = try {
    val field = classOf[ClassLoader].getDeclaredField("usr_paths")
    field.setAccessible(true)
    val paths = field.get(null).asInstanceOf[Array[String]]
    if(!(paths contains dir)) {
      field.set(null, paths :+ dir)
      System.setProperty("java.library.path",
        System.getProperty("java.library.path") +
          java.io.File.pathSeparator +
          dir)
    }
    true
  } catch {
    case _: IllegalAccessException =>
      println("Error. Insufficient permissions; can't modify private variables.")
      false
    case _: NoSuchFieldException =>
      println("Error. JVM implementation incompatible with path hack")
      false
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/7716455/how-should-i-remove-the-first-occurrence-of-an-object-from-a-list-in-scala
  def removeFirstOccurrence[A](xs: Seq[A])(p: A => Boolean) = {
    var found = false
    xs.filter(x => found || !p(x) || { found=true; false })
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/1157564/zipwith-mapping-over-multiple-seq-in-scala
  //return a list with the i element of each sequence
  def zipMultiple[K] (l:IndexedSeq[IndexedSeq[K]]) = {
    //-----------------------------------------------------------------------
    implicit class IterableOfIterablePimps[T](q: Iterable[Iterable[T]]) {
      //-----------------------------------------------------------------------
      def mapZipped[V](f: Iterable[T] => V): Iterable[V] = new Iterable[V] {
        override def iterator: Iterator[V] = new Iterator[V] {
          override def next(): V = {
            val v = f(itemsLeft.map(_.head))
            itemsLeft = itemsLeft.map(_.tail)
            v
          }
          override def hasNext: Boolean = itemsLeft.exists(_.nonEmpty)
          private var itemsLeft = q
        }
      }
      //-----------------------------------------------------------------------
    }
    //-----------------------------------------------------------------------
    l.mapZipped { group => group.toList }.toList
  }
  //---------------------------------------------------------------------------
  //for each value, get the position of the closest one
  def getComparablePairInt(backGroundSeq: IndexedSeq[Int] ): IndexedSeq[Int] = {
    for( i<- 0 until backGroundSeq.size) yield {
      val l = (for( k<- 0 until backGroundSeq.size)  yield { if(k != i) Some(backGroundSeq(k)) else None}).flatten
      val b  = backGroundSeq(i)
      val lDif = l map {v=> Math.abs( v - b )}
      val value= l( lDif.indexOf(lDif.min))
      (backGroundSeq.zipWithIndex map {case (v, pos)=> if ((v == value) && (pos != i)) Some(pos) else None }).flatten.head
    }
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/44672145/functional-way-to-find-the-longest-common-substring-between-two-strings-in-scala
  def longestCommonSubString(left: String, right: String) = {
    left.inits.flatMap(_.tails)
      .toList.sortBy(_.length).reverse
      .find(right.contains(_)).get
  }
  //---------------------------------------------------------------------------
  def getCoalescentIntIntervalSeq(v: Array[(Int,Int)]): Array[Array[(Int,Int)]] = {
    //-------------------------------------------------------------------------
    val r = ArrayBuffer[Array[(Int,Int)]]()
    //-------------------------------------------------------------------------
    def getFirstCoalescence(seq: Array[(Int,Int)]): Array[(Int,Int)] = {
      var endIndex = -1;
      if (seq.length == 0) return Array()
      if (seq.length == 1) return seq.slice(0,1)
      for (v<-seq.sliding(2)) {
        if ((v(0)._1 + 1) == v(1)._1) endIndex +=1
        else {
          if (endIndex == -1) return seq.slice(0,1)
          else return seq.slice(0,endIndex+2)
        }
      }
      seq.slice(0,endIndex+2)
    }
    //-------------------------------------------------------------------------
    var sortedV = v.sortWith( _._1 < _._1)
    while(true){
      val g = getFirstCoalescence(sortedV)
      r += g
      sortedV = sortedV.drop(g.length)
      if (sortedV.length == 0) return r.toArray
    }
    r.toArray
    //-------------------------------------------------------------------------
  }
  //--------------------------------------------------------------------------
  def groupByConsecutiveInt(a: Array[Int]) = {
    a.foldLeft(List.empty[Array[Int]]) { case (acc, n) =>
      if (acc.isEmpty) List(Array(n))
      else {
        if ((acc.last.last +1) == n)  {
          val last = acc.last
          acc.dropRight(1) :+ (last :+ n)
        }
        else  acc :+ Array(n)
      }
    }
  }
  //---------------------------------------------------------------------------
  //http://rosettacode.org/wiki/Power_set#Scala
  def getAllSubsets[A](s: Set[A]) = s.foldLeft(Set(Set.empty[A])) { case (ss, el) => ss ++ ss.map(_ + el) }
  //---------------------------------------------------------------------------
  def flatMapSubLists[A,B](ls: List[A])(f: List[A] => List[B]): List[B] =
    ls match {
      case Nil => Nil
      case sublist@(_ :: tail) => f(sublist) ::: flatMapSubLists(tail)(f)
    }
  //---------------------------------------------------------------------------
  def allIndexWhere [T](a: Array[T], p: T => Boolean ) =
    a.zipWithIndex.collect { case (elem, idx) if p(elem) => idx }
  //---------------------------------------------------------------------------
  //modified from
  //https://codereview.stackexchange.com/questions/112186/divide-list-into-n-parts-in-scala
  def repeatedSpan[T](iterable: Iterable[T], pred: T => Boolean): Iterable[Iterable[T]] = {
    @scala.annotation.tailrec
    def loop(accum: Vector[Iterable[T]], rest: Iterable[T]): Iterable[Iterable[T]] =
      rest.span(pred) match {
        case (Nil, doesNotMatch :: unTested) => loop(accum :+ List(doesNotMatch), unTested)
        case (matches, doesNotMatch :: unTested) => loop(accum :+ matches :+ List(doesNotMatch), unTested)
        case (matches, Nil) => accum :+ matches
        case _ =>   Seq(Seq())
      }
    loop(Vector(), iterable).filter( !_.isEmpty )
  }
  //---------------------------------------------------------------------------
  def repeatGroup[T] (iterable: Iterable[Iterable[T]],  pred: Iterable[T] => Boolean) = {
    //-------------------------------------------------------------------------
    val result =  ListBuffer[Iterable[T]]()
    val previousMatch = ListBuffer[T]()
    //-------------------------------------------------------------------------
    @scala.annotation.tailrec
    def loop(iterable: Iterable[Iterable[T]]): Unit = {
      iterable.span(pred) match {
        case (Nil, doesNotMatch :: unTested) =>
          result += previousMatch map (t=> t)
          previousMatch.clear
          result += doesNotMatch
          loop(unTested)
        case (matches, doesNotMatch :: unTested) =>
          previousMatch ++= matches.flatten
          result += previousMatch map (t=> t)
          previousMatch.clear
          result += doesNotMatch
          loop(unTested)
        case (matches, Nil) =>
          previousMatch ++= matches.flatten
          result += previousMatch map (t=> t)
          previousMatch.clear
        case _ =>
      }
    }
    //-------------------------------------------------------------------------
    loop(iterable)
    result.filter(!_.isEmpty).toArray
  }
  //---------------------------------------------------------------------------
  def hash(s: String) = md5(s.getBytes())
  //---------------------------------------------------------------------------
  //https://gist.github.com/stonegao/1044641
  def md5(d: Array[Byte]) = {
    val m = java.security.MessageDigest.getInstance("MD5")
    m.update(d, 0, d.length)
    new java.math.BigInteger(1, m.digest()).toString(16)
  }
  //-------------------------------------------------------------------------
  def isByte(s: String): Boolean = {
    try {s.toByte}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isShort(s: String): Boolean = {
    try {s.toShort}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isInteger(s: String): Boolean = {
    try {s.toInt}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isLong(s: String): Boolean = {
    try {s.toLong}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isFloat(s: String): Boolean = {
    try {s.toFloat}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isDouble(s: String): Boolean = {
    try {s.toDouble}
    catch { case _: Exception => return false}
    true
  }
  //-------------------------------------------------------------------------
  def isHexString(s: String): Boolean = {
    try {
      Integer.parseUnsignedInt(s,16)
    }
    catch {
      case _: Exception => return false
    }
    true
  }
  //-------------------------------------------------------------------------
  def getBooleanOrDefault(s: String, default: Boolean = false): Boolean = {
    if (s.isEmpty) return default
    if (s.toLowerCase() == "t") true
    else
      if (s.toLowerCase() == "1") true
      else
         if (s.toLowerCase() == "true") true
         else false
  }
  //-------------------------------------------------------------------------
  def getByteOrDefault(s: String, default: Byte = Byte.MinValue): Byte = {
    if (s.isEmpty) return default
    try {s.toByte}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  def getShortOrDefault(s: String, default: Short = Short.MinValue): Short = {
    if (s.isEmpty) return default
    try {s.toShort}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  def getIntOrDefault(s: String, default: Int = Int.MinValue): Int = {
    if (s.isEmpty) return default
    try {s.toInt}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  def getLongOrDefault(s: String, default: Long = Long.MinValue): Long = {
    if (s.isEmpty) return default
    try {s.toLong}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  def getFloatOrDefault(s: String, default: Float = Float.MinValue): Float = {
    if (s.isEmpty) return default
    try {s.toFloat}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  def getDoubleOrDefault(s: String, default: Double = Double.MinValue): Double = {
    if (s.isEmpty) return default
    try {s.toDouble}
    catch { case _: Exception => default}
  }
  //-------------------------------------------------------------------------
  //https://stackoverflow.com/questions/9938098/how-to-check-to-see-if-a-string-is-a-decimal-number-in-scala
  def isAllDigits(x: String) = x forall Character.isDigit
  //---------------------------------------------------------------------------
  def truncate(value: Double)  =  value - value % 1
  //---------------------------------------------------------------------------
  def isSorted[T](list: List[T])(implicit ord: Ordering[T]): Boolean = list match {
    case Nil => true
    case x :: xs => xs.headOption.fold(true)(ord.lteq(x, _)) && isSorted(xs)
  }
  //---------------------------------------------------------------------------
  def isConsecutiveListLong(seq: Seq[Long], isSorted: Boolean = false): Boolean = {
    val sotedSeq = if (!isSorted) seq.sorted else seq
    (sotedSeq.last - sotedSeq.head) == (seq.length - 1)
  }
  //---------------------------------------------------------------------------
  def mergeMap[K, V](m1: Map[K, V], m2: Map[K, V]): Map[K, List[V]] =
    (m1.keySet ++ m2.keySet) map { i => i -> (m1.get(i).toList ::: m2.get(i).toList) } toMap
  //---------------------------------------------------------------------------
  def buildConsecutiveByteSeq(byteSize: Long, startingValue: Byte = 0) =
    Array.tabulate(byteSize.toInt)(n => (n + startingValue).toByte)
  //---------------------------------------------------------------------------
  def buildConsecutiveByteSeqSeq(sequenceCount: Long
                                 , byteSize: Long
                                 , startingValue: Byte = 0) = {
    var sv = startingValue
    (for (_ <- 1L to sequenceCount) yield {
      val r = buildConsecutiveByteSeq(byteSize,sv)
      sv = (sv + byteSize).toByte
      r
    }).toArray
  }
  //---------------------------------------------------------------------------
  def getByteCount(l:Long) = (l / 8) + (if ((l % 8) > 0) 1 else 0)
  //---------------------------------------------------------------------------
  def bitStringToByteSeq(s: String) = new BigInteger(s, 2).toByteArray
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Util.scala
//=============================================================================
