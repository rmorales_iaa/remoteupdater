/**
  * Created by: Rafael Morales (rmorales@iaa.es) 
  * Date:  15/Nov/2018 
  * Time:  09h:52m
  * Description: None
  */
//=============================================================================
package com.common.util.pattern
//=============================================================================
//=============================================================================
import com.common.logger.VisualLazyLogger
import scala.util.matching.Regex
//=============================================================================
//=============================================================================
object Pattern extends VisualLazyLogger {
  //see more patterns  in: https://www.tutorialspoint.com/scala/scala_regular_expressions.htm
  //---------------------------------------------------------------------------
  final val INTEGER_PATTERN = List("[+-]?\\d+".r)
  //---------------------------------------------------------------------------
  final val FLOAT_PATTERN_1 : Regex = "[+-]?\\d+\\.\\d*([ED][+-]?\\d+)?".r //integer part must exits
  final val FLOAT_PATTERN_2 : Regex = "[+-]?\\d*\\.\\d+([ED][+-]?\\d+)?".r //fraction part must exits
  final val FLOAT_PATTERN = List(FLOAT_PATTERN_1, FLOAT_PATTERN_2)
  //---------------------------------------------------------------------------
  def parsePattern(s: String,
                   patternList: List[Regex],
                   errorS: String  = "",
                   isWarning: Boolean = false,
                   reportErrorMessage: Boolean = true
                  ): Boolean = {

    patternList.foreach { pattern ⇒
      val w = pattern.findFirstIn(s)
      if (w.isDefined && (w.get.length == s.length))
        return true
    }
    if (isWarning) warning(errorS)
    else if (reportErrorMessage) error(errorS)
    false
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Pattern.scala
//=============================================================================
