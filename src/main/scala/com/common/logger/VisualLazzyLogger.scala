/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  25/Aug/2018
  * Time:  12h:04m
  * Description:
  * // add custom visualLogger levels to log4j version 2
  * https://logging.apache.org/log4j/2.x/manual/configuration.html
  * https://logging.apache.org/log4j/2.x/manual/customloglevels.html
  */
//=============================================================================
package com.common.logger
//=============================================================================
import org.apache.logging.log4j.{LogManager, Logger}
//=============================================================================
//=============================================================================
trait VisualLazyLogger extends VisualLoggerBase {
   protected lazy val logger: Logger = LogManager.getLogger(getClass.getName)
}
//=============================================================================
//=============================================================================
//End of file VisualLazyLogger
//=============================================================================
