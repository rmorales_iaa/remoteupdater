//=============================================================================
package com.common.logger
//=============================================================================
import java.io.File

import org.apache.logging.log4j.{Level, Logger}
//=============================================================================
//=============================================================================
trait VisualLoggerBase {
  //---------------------------------------------------------------------------
  private final val tagPrefix: String = ""
  private final val levelValue: Int = 250 //official values: WARN=300 and ERROR=200

  private final val outputDirectory: String = "output/logger"
  private final val propertyFileName: String = "input/log4j2.xml"

  protected val logger: Logger
  //---------------------------------------------------------------------------
  //tags
  //---------------------------------------------------------------------------
  final val FATAL_TAG = tagPrefix + "FATAL"
  final val ERROR_TAG = tagPrefix + "ERROR"
  final val WARN_TAG =  tagPrefix + "WARN "
  final val INFO_TAG =  tagPrefix + "INFO "
  //---------------------------------------------------------------------------
  //levels
  //---------------------------------------------------------------------------
  val levelFatal: Level = Level.forName(FATAL_TAG,levelValue)
  val levelError: Level = Level.forName(ERROR_TAG,levelValue+1)
  val levelWarning: Level = Level.forName(WARN_TAG,levelValue+2)
  val levelInfo: Level = Level.forName(INFO_TAG,levelValue+3)
  val levelDebug: Level = Level.forName(INFO_TAG,levelValue+4)
  //---------------------------------------------------------------------------
  //methods
  //---------------------------------------------------------------------------
  //use when logger is visual
  /*
  import com.common.gui.VisualMainFrame.visualLogger
  def fatal(s: String): Boolean = { logger.log(levelFatal, s); if(visualLogger != null) visualLogger.fatal(s); false }
  def error(s: String): Boolean = { logger.log(levelError, s); if(visualLogger != null) visualLogger.error(s); false }
  def exception(e: Exception, s: String): Boolean = {  logger.log(levelError, e.toString + s); if(visualLogger != null) visualLogger.exception(e, s); false }
  def warning(s: String): Boolean = { logger.log(levelWarning, s); if(visualLogger != null) visualLogger.warning(s); true}
  def info(s: String): Boolean =    { logger.log(levelInfo, s); if(visualLogger != null) visualLogger.info(s); true}
  def debug(s: String): Boolean =   { logger.log(levelDebug, s); if(visualLogger != null) visualLogger.debug(s); true}
*/
  //use when logger is only text
  def fatal(s: String): Boolean =   { logger.log(levelFatal, s); false }
  def error(s: String): Boolean =   { logger.log(levelError, s); false }
  def exception(e: Exception, s: String): Boolean = { logger.log(levelError, " " + e.toString + s); false }
  def warning(s: String): Boolean = { logger.log(levelWarning, s); true }
  def info(s: String): Boolean =    { logger.log(levelInfo, s); true }
  def debug(s: String): Boolean =   { logger.log(levelDebug, s); true}
  //---------------------------------------------------------------------------
  private def createDirectoryIfNotExist(s: String): Unit = {
    val dir = new File(s)
    if (!dir.exists) dir.mkdirs
  }
  //---------------------------------------------------------------------------
  System.setProperty("log4j.configurationFile",propertyFileName)

  //https://stackoverflow.com/questions/27361052/failing-to-load-log4j2-while-running-fatjar
  System.setProperty("log4j2.loggerContextFactory", "org.apache.logging.log4j.core.impl.Log4jContextFactory")
  createDirectoryIfNotExist(outputDirectory)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file VisualLoggerBase.scala
//=============================================================================
