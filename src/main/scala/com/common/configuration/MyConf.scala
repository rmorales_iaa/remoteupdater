//=============================================================================
//File: MyConf.scala
//=============================================================================
/** It manages the user configuration file in commonHenosis. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package sectionName
//=============================================================================
package com.common.configuration
//=============================================================================
// System import sectionName
//=============================================================================
import com.common.logger.VisualLazyLogger
import com.typesafe.config.{ConfigRenderOptions, ConfigValueFactory}
import collection.JavaConverters._
//=============================================================================
import java.io.File
//=============================================================================
// User import sectionName
//=============================================================================
import com.typesafe.config.{Config, ConfigFactory}
//=============================================================================
// Class/Object implementation
//=============================================================================
/** Parser of a user's configuration file based on 'config' ([[https://github.com/typesafehub/config]])
 *  The configuration files are based on key-value pairs
 */
object MyConf {
  //---------------------------------------------------------------------------
  var c :  MyConf = null
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
case class MyConf(confFileName: String = "input/configuration/main.conf", verbose: Boolean = false) extends VisualLazyLogger {
  //---------------------------------------------------------------------------
  //Class variables
  //---------------------------------------------------------------------------
  private var config : Config = _
  private var loaded = false
  private val FileSeparator = java.io.File.separator // '/' in gnu-linux
  private val LineSeparator = System.getProperty("line.separator","\n")
  //---------------------------------------------------------------------------
  load(verbose)
  //---------------------------------------------------------------------------
  def isLoaded: Boolean = loaded
  //---------------------------------------------------------------------------
  private def load(verbose: Boolean = false) : Unit = {
    if(!isLoaded) {
      if (!loadAndCheck(verbose))
        fatal(s"Error loading the configuration file: '$confFileName'")
    }
  }
  //---------------------------------------------------------------------------
  /** Loads and checks a user's  configuration file
    @return True if file has been properly parsed
            or false if file has not been properly parsed */
  private def loadAndCheck(verbose: Boolean = false) : Boolean = {
    try{
     if (verbose) info(s"Loading configuration file: '$confFileName'")
     val f = new File(confFileName)
     if (!f.exists() || f.isDirectory)
       return error(s"Configuration file: '$confFileName' does not exist")
     config = ConfigFactory.parseFile(new File(confFileName)).resolve()
     config.checkValid(ConfigFactory.defaultReference(),confFileName)
     if (verbose) info(s"Loaded configuration file: '$confFileName'")
     loaded = true
     true
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration file: '$confFileName'")
      false
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a boolean value associated to a key in the current configuration file
    @param key Name of the key
    @return The boolean value associated with the key
  */
  def getBoolean(key: String) : Boolean = {

    try{
      config.getBoolean(key)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration boolean key entry: '$key'")
      false
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a string value associated to a key in the current configuration file
    @param key Name of the key
    @return The string value associated with the key
  */
  def getString(key: String) : String = {
    try{
     config.getString(key)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration string key entry: '$key'")
      ""
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a byte value associated to a key in the current configuration file
  @param key Name of the key
  @return The integer value associated with the key
   */
  def getByte(key: String) : Byte = {

    try{
      config.getInt(key).toByte
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration int key entry: '$key'")
        -1
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a integer value associated to a key in the current configuration file
    @param key Name of the key
    @return The integer value associated with the key
  */
  def getInt(key: String) : Int = {

    try{
     config.getInt(key)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration int key entry: '$key'")
      -1
    }
  }

  //---------------------------------------------------------------------------
  /** Returns a double value associated to a key in the current configuration file
    @param key Name of the key
    @return The float value associated with the key
  */
  def getFloat(key: String) : Float = getDouble(key).toFloat

  //---------------------------------------------------------------------------
  /** Returns a double value associated to a key in the current configuration file
    @param key Name of the key
  @return The double value associated with the key
   */
  def getDouble(key: String) : Double = {

    try{
      config.getDouble(key)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration double key entry: '$key'")
        -1
    }
  }

  //---------------------------------------------------------------------------
  /** Returns a list of strings associated to a key in the current configuration file
    @param key Name of the key
    @return The list of strings associated with the key
  */
  //Sample x=["u","b"]

  def getStringSeq(key: String) : List[String] = {
    try{
      config.getStringList(key).asScala.toList
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration string list key entry: '$key'")
      List()
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a list of integers associated to a key in the current configuration file
    @param key Name of the key
  @return The list of integers associated with the key
    */
  //Sample x=[1,-5]
  def getIntSeq(key: String) : List[Int] = {

    try {
      config.getIntList(key).asScala.toList.map(_.toInt)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration int list key entry: '$key'")
        List()
    }
  }
  //---------------------------------------------------------------------------
  /** Returns a list of integers associated to a key in the current configuration file
  @param key Name of the key
  @return The list of integers associated with the key
   */
  //Sample x=[3.2,-6.3131313131313135e-002]
  def getDoubleSeq(key: String) : List[Double] = {

    try{
      config.getDoubleList(key).asScala.toList.map(_.toDouble)
    }
    catch {
      case e: Exception => exception(e, s" Error parsing configuration int list key entry: '$key'")
        List()
    }
  }
  //---------------------------------------------------------------------------
  def getStringWithDefaultValue(key: String, defaultValue: String) : String = {
    var r : String = defaultValue
    if (existKey(key)) {
      try { r = config.getString(key) }
      catch {
        case e: Exception => exception(e, s" Error parsing configuration int key entry: '$key'")
      }
    }
    r
  }
  //---------------------------------------------------------------------------
  def getIntWithDefaultValue(key: String, defaultValue: Int) : Int = {
    var r : Int = defaultValue
    if (existKey(key)) {
      try { r = config.getInt(key) }
      catch {
        case e: Exception => exception(e, s" Error parsing configuration int key entry: '$key'")
      }
    }
    r
  }
  //---------------------------------------------------------------------------
  def getDoubleWithDefaultValue(key: String, defaultValue: Double) : Double = {
    var r : Double = defaultValue
    if (existKey(key)) {
      try { r = config.getDouble(key) }
      catch {
        case e: Exception => exception(e, s" Error parsing configuration int key entry: '$key'")
      }
    }
    r
  }
  //---------------------------------------------------------------------------
  private def fileExist(s:String) : Boolean = {
    val f = new File(s)
    f.exists && !f.isDirectory
  }
  //---------------------------------------------------------------------------
  import java.io.{BufferedWriter, File, FileWriter}
  import scala.io.Source
  import scala.language.postfixOps
  import sys.process._

  def updateKey(path : String //absolute_path
    ,  parent : String  
    ,  key : String
    ,  newValue : String): Boolean = {
    
    if (getString(parent+"."+key) == newValue) return true//same value, do nothing
      
    if (!fileExist(path))
      return error(s"Error.Can not updateValue key '$key' because file 'path' does not exist")

    def f=new File(path)
    val tempFileName= f.getParentFile + FileSeparator+"next_"+f.getName
    val tempF = new BufferedWriter(new FileWriter(new File(tempFileName)))
   
    Source.fromFile(f).getLines.map { line =>
        val list = line.split("=")
        if ((list.size == 2) && line.split("=")(0).trim.contains(key.trim))
          line.substring(0,line.indexOf("=")+1)+" \""+newValue+"\""
        else 
          line
      }
      .foreach(x => tempF.write(x + LineSeparator))
    tempF.close()
    f.delete
    
    //rename new file
    if(System.getProperty("os.name").toLowerCase.contains("win")) "myWinRename.bat" !
    else new java.io.File(tempFileName).renameTo(f)

    true
  }
  //---------------------------------------------------------------------------
  def prettyPrintConcise(): Unit =
    println(config.root().render(ConfigRenderOptions.concise()))
  //---------------------------------------------------------------------------
  def prettyPrintDefaults(): Unit =
    println(config.root().render(ConfigRenderOptions.defaults()))
  //---------------------------------------------------------------------------
  def existKey(key: String) : Boolean = {
    try {
      config.hasPath(key)
    }
    catch {
      case e: Exception  => exception(e, s" Error parsing configuration string key entry: '$key'")
      case _ : Throwable => true
    }
  }
  //---------------------------------------------------------------------------
  def getIfExistKey(key: String) : Option[String] = {
    if (existKey(key)) Some(getString(key))
    else None
  }
  //---------------------------------------------------------------------------
  def withValue(root: String, v: String) =  config.withValue(root, ConfigValueFactory.fromAnyRef(v))
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
} //End of object 'MyConf'
//=============================================================================
// End of 'MyConf.scala' file
//=============================================================================
