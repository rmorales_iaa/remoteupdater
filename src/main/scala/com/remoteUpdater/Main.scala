/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Oct/2019
 * Time:  10h:33m
 * Description: muser : MUSe inspEctorc
 */
//=============================================================================
package com.remoteUpdater
//=============================================================================
import java.time.Instant
import com.common.git.GitManager
import com.common.logger.VisualLazyLogger
//=============================================================================
import com.common.util.time.Time._
import com.common.configuration.MyConf
//=============================================================================
object Main extends VisualLazyLogger {
  //---------------------------------------------------------------------------
  //timing
  val startMs = System.currentTimeMillis()
  //---------------------------------------------------------------------------
  //default configuration file
  val defaultConfigurationFile  = "input/configuration/main.conf"
  //---------------------------------------------------------------------------
  //Version
  private final val MAJOR_VERSION = 0
  private final val MINOR_VERSION = 0
  private final val COMPILATION_VERSION = 1
  private final val DATE_VERSION = "14 December 2022"
  final val VERSION = s"$MAJOR_VERSION.$MINOR_VERSION.$COMPILATION_VERSION"
  final val VERSION_AND_DATE = s"$VERSION ($DATE_VERSION)"
  final val AUTHOR="Rafael Morales (rmorales [at] iaa [dot] com)"
  final val FILIATION="UDIT. IAA-CSIC. 2019-2020"
  final val LICENSE="http://www.apache.org/licenses/LICENSE-2.0.txt"
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    MyConf.c = MyConf(defaultConfigurationFile,verbose = true)
    if (!MyConf.c.isLoaded){
      fatal(s"Can not load the default configuration file $defaultConfigurationFile")
      System.exit(-1)
    }
    showInfo
  }
  //---------------------------------------------------------------------------
  private def showInfo(): Unit ={
    info(s"""
  Remote updater
    Version  : $VERSION_AND_DATE
    Author   : $AUTHOR
    Filiation: $FILIATION
    License  : $LICENSE\n""")
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {
    warning(s"RemoteUpdater elapsed time: ${getFormattedElapseTimeFromStart(startMs)} ms " )
  }
  //---------------------------------------------------------------------------
  private def remoteUpdate () = {
    val remoteRepoURL = MyConf.c.getString("RemoteUpdate.remoteRepository.remoteURL")
    val remoteRepoToken = MyConf.c.getString("RemoteUpdate.remoteRepository.token")
    val remoteRepoUserName = MyConf.c.getString("RemoteUpdate.remoteRepository.userName")
    val localRepoPath = MyConf.c.getString("RemoteUpdate.localRepository.localPath")

    if (!MyConf.c.getBoolean("RemoteUpdate.use")) info("Ignoring remote repository updates")
    else GitManager(localRepoPath, remoteRepoURL, remoteRepoUserName, remoteRepoToken)
  }
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {

    initialActions

    remoteUpdate()

    finalActions
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file Main.scala
//=============================================================================
